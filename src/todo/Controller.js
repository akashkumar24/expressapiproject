const yup = require("yup");
const queris = require("./queries");
const { PrismaClient } = require("@prisma/client");

const prisma = new PrismaClient();

const todoSchema = yup.object().shape({
  text: yup.string().required(),
  iscompleted: yup.boolean().required(),
});
const paramSchema = yup.object().shape({
  id: yup.number().required(),
});

// todo list API
const getTodos = async (req, res, next) => {
  try {
    const data = await prisma.todos.findMany();
    if (data.length === 0) {
      res.status(404).json({ message: "No todos found" });
    } else {
      res.json(data);
    }
  } catch (error) {
    next(error);
  }
};

//todo by id
const getTodoById = async (req, res, next) => {
  try {
    const id = parseInt(req.params.id);
    if (isNaN(id) || req.params.id.trim() === "") {
      res.status(400).json({ error: "Invalid todo id" });
      return;
    }

    const todo = await prisma.todos.findUnique({ where: { id } });
    if (!todo) {
      res.status(404).send("Not Found");
    } else {
      res.status(200).json(todo);
    }
  } catch (error) {
    next(error);
  }
};

const createTodo = async (req, res, next) => {
  try {
    await todoSchema.validate(req.body);

    const todo = await prisma.todos.create({
      data: {
        text: req.body.text,
        iscompleted: req.body.iscompleted,
      },
    });
    res.status(201).json(todo);
  } catch (error) {
    next(error);
  }
};

const updatedTodo = async (req, res, next) => {
  try {
    await paramSchema.validate(req.params);

    const id = parseInt(req.params.id);
    await todoSchema.validate(req.body);
    const { text, iscompleted } = req.body;
    const todo = await prisma.todos.findUnique({ where: { id: id } });
    if (!todo) {
      res.status(404).send({ message: "No todo found" });
    } else {
      const updatedTodo = await prisma.todos.update({
        where: { id: id },
        data: { text: text, iscompleted: iscompleted },
      });
      res.status(200).json(updatedTodo);
    }
  } catch (error) {
    next(error);
  }
};

const deleteTodoById = async (req, res, next) => {
  try {
    await paramSchema.validate(req.params);
    const id = parseInt(req.params.id);
    const todo = await prisma.todos.findUnique({
      where: { id },
    });
    if (!todo) {
      res.status(404).send("No Todo found");
    } else {
      const del = await prisma.todos.delete({
        where: { id },
      });
      res.status(200).send("Todo removed Successfully");
    }
  } catch (error) {
    next(error);
  }
};

module.exports = {
  getTodos,
  getTodoById,
  createTodo,
  updatedTodo,
  deleteTodoById,
};
