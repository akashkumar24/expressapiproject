const { Router } = require("express");
const controller = require("./Controller");
const router = Router();

router.get("/", controller.getTodos);
router.get("/:id", controller.getTodoById);
router.post("/", controller.createTodo);
router.put("/:id", controller.updatedTodo);
router.delete("/:id", controller.deleteTodoById);

module.exports = router;
