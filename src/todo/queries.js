const getTodoList = "SELECT * FROM todos";
const getSpecificTodo = "SELECT * FROM todos WHERE id = $1";
const checkIdExits = "SELECT id FROM todos WHERE id = $1";
const addTodo = "INSERT INTO todos (text, iscompleted) VALUES ($1,$2)";
const updateTodo =
  "UPDATE todos SET text=$1, iscompleted=$2 WHERE id = $3 returning id,text,iscompleted";
const deleteTodoById = "DELETE FROM todos WHERE id = $1";

module.exports = {
  getTodoList,
  getSpecificTodo,
  checkIdExits,
  addTodo,
  updateTodo,
  deleteTodoById,
};
