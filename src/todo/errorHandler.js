const errorHandler = (error, req, res, next) => {
  console.error(error);
  if (error.name === "ValidationError") {
    res.status(400).json({ error: error.message });
  } else {
    res.status(500).json({ message: "Internal server error" });
  }
};

module.exports = errorHandler;
