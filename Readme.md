# Pre-requisites for the project.

- Node.js
- Postgres
- express

# to run the code.

## steps to create postgres database and table

login into the postgre server using username and password.

### run the following commands in the terminal

CREATE DATABASE dbname;
CREATE TABLE todos (ID SERIAL PRIMARY KEY, text varchar(255), isCompleted BOOLEAN)
INSERT INTO todos (text,isCompleted)
VALUES('Build the Express JS project', false);

just install the npm i.
to download all the dependencies related to the project.

install yub library. by using command npm install yub.

this project uses prisma ORM to interact with databases.

# description of the working of files.

1)server.js listens for te incoming requests.
2)router module exports an instance of aan express.js router object.router object handles HTTP requests based on requested path and http method.the last lines are for various HTTP routes.

3)controller.js uses prisma ORM to comunicate with the database
->todoschemas and paramschema variable use the yub libraries for data validation..

todoschemas defined then shape of the todo item.
paramschema validates the id parameter.

controller.js has various asynchronous functions which handles the http requests uses a try catch block to handle errors.

this code uses middleware error handler.js to handle the error.
