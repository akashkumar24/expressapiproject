const express = require("express");
const todoRoutes = require("./src/todo/routes");
const errorHandler = require("./src/todo/errorHandler.js");
const app = express();

app.use(express.json());

app.get("/", (req, res) => {
  res.send("Hello World");
});

app.use("/todos", todoRoutes);
app.use(errorHandler);
app.listen(process.env.EXPRESS_JS_PORT || 3000, () =>
  console.log(`app listening 7832 or 3000 `)
);
